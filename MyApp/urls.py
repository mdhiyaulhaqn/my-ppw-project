from django.conf.urls import url
from .views import about, academic, skill, contact, form
app_name = "MyApp"
urlpatterns = [
    url(r'^$', about, name='about'),
    url(r'^academic', academic, name='academic'),
    url(r'^skill', skill, name='skill'),
    url(r'^contact', contact, name='contact'),
    url(r'^form', form, name='form'),
]
