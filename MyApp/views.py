from django.shortcuts import render
from django.http import HttpResponse

def about(request):
    return render(request, 'MyWeb.html')

def academic(request):
    return render(request, 'MyWeb2.html')

def skill(request):
    return render(request, 'MyWeb3.html')

def contact(request):
    return render(request, 'MyWeb4.html')

def form(request):
    return render(request, 'MyWeb5.html')


