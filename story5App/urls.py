from django.conf.urls import url
from django.urls import path
from .views import *

app_name = "story5App"
urlpatterns = [
    url(r'^forms_post', forms_post, name='forms_post'),
    url(r'^forms_collection', forms_collection, name='forms_collection'),
    url(r'^forms_html', forms_html, name='forms_html'),
    url(r'^forms_delete', forms_delete, name='forms_delete')
]