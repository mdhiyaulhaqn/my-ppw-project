from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import formsModels
# Create your views here.
def forms_post(request):
    response = {}
    if(request.method == 'POST' or None):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        forms = formsModels(name=response['name'], date=response['date'], place=response['place'], category=response['category'])
        forms.save()
        html = 'Schedule_Forms.html'
        return HttpResponseRedirect('forms_collection')
        #return render(request, html, response)
    else:
        return HttpResponseRedirect('forms_post')

def forms_collection(request):
    allForms = formsModels.objects.all()
    response = {'result': allForms}
    html = 'table.html' 
    return render(request, html, response)

def forms_html(request):
    response = {'forms' : Schedule_Form}
    return render(request, "Schedule_Forms.html", response)

def forms_delete(request):
    allForms = formsModels.objects.all().delete()
    return HttpResponseRedirect('forms_collection')