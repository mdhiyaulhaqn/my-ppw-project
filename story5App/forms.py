from django import forms

class Schedule_Form(forms.Form):
    attrs = {'class': 'form-control'}
    date = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), input_formats=['%Y-%m-%dT%H:%M'])
    name = forms.CharField(label='Nama', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Tempat', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Kategori', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))