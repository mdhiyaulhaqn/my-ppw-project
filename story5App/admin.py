from django.contrib import admin

# Register your models here.
from .models import formsModels

admin.site.register(formsModels)