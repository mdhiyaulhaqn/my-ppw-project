from django.db import models

class formsModels(models.Model):
    name = models.CharField(max_length=27)
    date = models.DateTimeField()
    place = models.CharField(max_length=27)
    category = models.CharField(max_length=27)
    